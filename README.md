
#Node JS API starter

This is an opinionated setup for a RESTful API on NodeJS

##Prerequisites

The generator assumes that you have the following software on your system.

* NodeJS + npm + Yeoman (the usual dependencies for running Yeoman)


The project that is generated with this starter depends on the following software:

* vagrant


##Installing the generator

Installing this Yeoman Generator pretty straightforward. 

* git clone this repository into a dedicated folder for your generator

* cd into the directory of the repository

* run `npm link` (use sudo if necessary)

From now on this generator is available 


##Using the generator to create a new project

* create a new folder for your project

* run `yo nodeapi`

* follow the steps in the wizard


##Using the generator to create a new API resource

The generator can be used to automatically create resources on the API. 

* Type `yo nodeapi:resource`

###Naming the resource
You can name your resources according to the RESTful conventions e.g. Project, Book, Product, Post etc. 
The resource will automatically create controllers and other necessary classes according to RESTful naming conventions.

###Optional: Using the Repository pattern
The resource-generator will ask if you want to use the Repository pattern. If you choose 'yes' it will create an extra layer 
between the REST controller and the model, a repository. This can be useful for having access to your models from within other 
parts of your application without contaminating the controller too much. Controllers will only handle HTTP requests and responses.
If you don't choose to use repositories, the controller will directly communicate with the model class.

###Optional: Requiring authentication on the resource
If you choose to require authentication on the REST resource it will use the basic authentication middleware. It follows the standard pattern
where GET is not authenticated, but PUT,POST and DELETE are. If you want more granular rules for authentication you can add them to your controllers.
This feature will be extended in future versions of this starter.

## Application architecture
The main architecture of the application is very flat. The exposed API controllers are all together in the api folder. Most of the business logic of
the application resides in the 'core'. The main reason for this is that it's immediately clear which resources are available on the API. The rest of 
the application is could have more cross-referencing, but API controllers only handle the HTTP-layer. The core framework takes care of the more
complicated logic of aggregating the right data in services, repositories, models etc. You can enhance this logic at will. 

```
└───app/
    |───api/
    │   └───module
    |   │   └───ModuleController.js
    |   └───module
    └───config/
    └───core/
        ├───base/
        ├───constants/
        ├───models/
        ├───repositories/
        ├───services/
     ───routes.js
     ───app.js
```


## Contributing
Contributors are welcome, please fork and send pull requests! If you find a bug or have any ideas on how to improve this project please submit an issue.