'use strict'

let ResourceRepository = require('../base/repositories/ResourceRepository')
let <%=modelName %> = require('../models/<%= modelName %>')
const <%=modelName %>Repository = () => {
    return Object.assign(ResourceRepository,{
        _model: <%=modelName%>
    })
}

module.exports = <%=modelName %>Repository()