'use strict'
let ResourceController = require('../../core/base/controllers/ResourceController')

let <%=modelName %> = require('../../core/<%=modelPath%>/<%=modelName %><% if(useRepository) {%>Repository<% } %>')

const <%=modelName %>Controller = () => {
    return Object.assign(Object.create(ResourceController),{
        _resource: <%=modelName %>
    })
}

module.exports = <%=modelName %>Controller()