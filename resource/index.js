'use strict'

const Generator = require("yeoman-generator")
const yeoman = require("yeoman-environment")
const pluralize = require("pluralize")

const env = yeoman.createEnv()

module.exports = class extends Generator {

    constructor(args, opts) {
        super(args, opts)

        this.argument('appname', {type: String, required: false});
        this.option('babel')
        this.answers = {}
    }

    prompting(){
        return this.prompt([
            {
                type    : 'input',
                name    : 'resource',
                message : 'Name your resource (singular), e.g. Product, Post, Message etc.'
            },
            {
                type    : 'confirm',
                name    : 'useRepository',
                message : 'Do you want your resource to use the Repository Pattern? (i.e. create a Repository in the core)'
            },
            {
                type    : 'confirm',
                name    : 'useAuthentication',
                message : 'Does your resource require authentication?'
            }
        ]).then((answers) => {

            this.props = answers

            const resourceName = this.props.resourceSingle = this.props.resource.toLowerCase()
            this.props.resourcePlural = pluralize(this.props.resourceSingle)
            this.props.modelName = resourceName.substr(0,1).toUpperCase() + resourceName.substr(1,resourceName.length).toLowerCase()
            return
        })
    }

    copyModel(){
        this.fs.copyTpl(
            this.templatePath('./Model.js'),
            this.destinationPath(`./app/core/models/${this.props.modelName}.js`),
            this.props
        )
    }

    writing(){
        this.props.modelPath = this.props.useRepository ? 'repositories' : 'models'

        if(this.props.useRepository)
            this.fs.copyTpl(
                this.templatePath('./Repository.js'),
                this.destinationPath(`./app/core/repositories/${this.props.modelName}Repository.js`),
                this.props
            )
        this.fs.copyTpl(
            this.templatePath('./Controller.js'),
            this.destinationPath(`./app/api/${this.props.resourcePlural}/${this.props.modelName}Controller.js`),
            this.props
        )
    }
    
    addRoutes(){
        const hook = "/* yeoman hook */",
              path = "app/api/resources.js",
              insert = `{name: '${this.props.resourcePlural}', controller: require('./${this.props.resourcePlural}/${this.props.modelName}Controller'),authenticated:${this.props.useAuthentication}},`
        let file = this.fs.read(path)

        this.fs.write(path, file.replace(hook, insert+'\n  '+hook))
    }

}
