'use strict'

const Generator = require("yeoman-generator")
const yeoman = require("yeoman-environment")

const env = yeoman.createEnv()

module.exports = class extends Generator {

	constructor(args,opts){
		super(args,opts)

		this.argument('appname', { type: String, required: false });
		this.option('babel')
		this.answers = {}
	}

	prompting(){
		return this.prompt([
			{
				type    : 'input',
				name    : 'name',
				message : 'Your project name',
				default : this.options.appname
			}, 
			{
				type    : 'input',
				name    : 'author',
				message : 'Your name',
				default : 'Robin Janssen'
			},
			{
				type    : 'input',
				name    : 'description',
				message : 'App description',
				default : ''
			},
			{
				type    : 'input',
				name    : 'repository',
				message : 'Git repository',
				default : ''
			},
			{
				type    : 'input',
				name    : 'dbName',
				message : 'Name your database',
				default : 'mydb'
			}
		]).then((answers) => {
			this.answers = answers
			this.answers.appname = this.answers.name.toLowerCase().replace(/ /g,'-')

			return
			
	    })
	}

	copyConfig(){
		const confs = ['.editorconfig','.gitignore']

		confs.forEach(conf=>{
			this.fs.copyTpl(
				this.templatePath(conf),
				this.destinationPath(conf),
				this.answers
			)
		})
	}
	copyWebapp(){
		this.fs.copyTpl(
			this.templatePath('.'),
			this.destinationPath('.'),
			this.answers
		)
	}

	runInstalls(){
		this.npmInstall()
	}

}