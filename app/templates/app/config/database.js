'use strict'

function conf(){
    switch(process.env.NODE_ENV){
        case 'DEV':
            return {
                db_host: "localhost",
                db_name: "<%= dbName %>"
            }
        case 'Test':
            return {
                db_host: "localhost",
                db_name: "<%= dbName %>"
            }
        case 'Acceptance':
        case 'Production':
            return {
                db_host: "localhost",
                db_name: "<%= dbName %>"
            }
        default:
            return {
                db_host: "localhost",
                db_name: "<%= dbName %>"
            }
    }
}

module.exports = conf()
