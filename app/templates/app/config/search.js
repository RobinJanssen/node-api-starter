'use strict'

module.exports = {
    searchengine: 'elasticsearch',

    'elasticsearch' : {
        host:'localhost:9200'
    },
    'mongodb' : {
        host: 'localhost:27017',
    }
}