module.exports = {
    languages: [
        'nl',
        'en',
        'de',
        'fr'
    ],
    default_locale: 'en'
}