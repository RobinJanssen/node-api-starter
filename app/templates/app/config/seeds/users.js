var mongoose = require('mongoose');
mongoose.connect('localhost:27017/2dor3d');

var User = require('../../core/models/User');
var UserRole = require('../../core/models/UserRole');

var users = [
	{
		username: "admin",
		first_name: "Robin",
		last_name: "Janssen"
	}
];

var i = 0;
var numUsers = users.length;


(function saveUsers(next){

	if(i<numUsers){

		var user = new User(users[i]);

		UserRole.findOne({name:"admin"}, function(err, role){
			user.role = {
				role_id: role._id,
				name: role.name
			};
			user.setPassword('admin', function() {
				user.save(function(err, data){

					if(err) console.log(err);
					console.log('Saved user', data);
					i++;

					saveUsers(next);
				});
			});
		});

	} else {
		next();
	}
	

})(end);

function end(){
	process.exit();
}

