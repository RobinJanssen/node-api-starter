var mongoose = require('mongoose');
mongoose.connect('localhost:27017/2dor3d');

var UserRole = require('../../core/models/UserRole');

var roles = [
	"editor",
	"viewer",
	"admin"
];

var i;
var numRoles = roles.length;

for(i = 0; i<numRoles; i++){
	var role = new UserRole({name:roles[i]});
	role.save(function(err, data){
		if(err) console.log(err);
		console.log('Saved user role', data);
	});
}

