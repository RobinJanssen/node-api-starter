'use strict'
let mongoose = require('mongoose');
mongoose.connect('localhost:27017/2dor3d');

let Q = require('q')

let Movie = require('../../core/models/Movie');

let movies =  [
    {
        title: "Start Wars: Rogue One",
        release_date: {
            year: 2017,
            month: 9
        },
        votes: {
            up_votes: 234,
            down_votes: 102
        },
        in_theaters: true,
        in_stores: false,
        is_released: true,
        main_image: 'rogue_one.jpg'
    },
    {
        title: "Start Wars: Rogue One",
        release_date: {
            year: 2017,
            month: 9
        },
        votes: {
            up_votes: 234,
            down_votes: 102
        },
        in_theaters: true,
        in_stores: false,
        is_released: true,
        main_image: 'rogue_one.jpg'
    }
]
let promises = movies.map(movie=>{
    return Movie.create(movie)
})

Q.all(promises)
    .then(s=>{
        console.log('seeded')
        process.exit()
    })