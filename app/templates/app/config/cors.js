'use strict';

module.exports = function setHeaders(req, res, next){
    var origin;
    switch(process.env.NODE_ENV){
        case 'DEV':
            origin = 'http://localhost:8080'
            break
        case 'Test':
            origin = 'http://localhost:8080'
            break
        case 'Acceptance':
        case 'Production':
            origin = 'http://localhost:8080'
            break
        default:
            origin = 'http://localhost:8080'
            break;
    }

    console.log(origin);

    res.header('Access-Control-Allow-Origin', origin);
    res.header("Access-Control-Allow-Credentials", "true");
    res.header('Access-Control-Allow-Methods', "GET,PUT,POST,DELETE,PATCH,OPTIONS");
    res.header('Access-Control-Allow-Headers', "Content-Type");

    if(req.method === 'OPTIONS')
        res.sendStatus(200)
    else
        next()
}