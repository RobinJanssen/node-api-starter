'use strict'

function conf(){
    switch(process.env.NODE_ENV){
        case 'DEV':
            return {
                secret: "Ahj333434dhj3434jfhj3454hgdhlgilpk4554popgf"
            }
        case 'Test':
        case 'Acceptance':
        case 'Production':
            return {
                secret: "Ahj333434dhj3434jfhj3454hgdhlgilpk4554popgf"
            }
        default:
            return {
                secret: "Ahj333434dhj3434jfhj3454hgdhlgilpk4554popgf"
            }
    }
}

module.exports = conf()
