'use strict'

function conf(){
    switch(process.env.NODE_ENV){
        case 'DEV':
            return {
                base_url: "http://localhost:8001",
                uploads: "./app/public/uploads"
            }
            break
        case 'Test':
            return {
                base_url: "http://localhost:8001",
                uploads: "./app/public/uploads"
            }
            break
        case 'Acceptance': //Fall through
        case 'Production':
            return {
                base_url: "http://localhost:8001",
                uploads: "./app/public/uploads"
            }
            break
        default:
            return {
                base_url: "http://localhost:8001",
                uploads: "./app/public/uploads"
            }
            break
    }
}

module.exports = conf()
