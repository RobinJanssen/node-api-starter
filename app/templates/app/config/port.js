'use strict'

function conf(){
    switch(process.env.NODE_ENV){
        case 'DEV':
            return 8001
        case 'Test':
            return 9094
        case 'Acceptance':
        case 'Production':
            return 9098
        default:
            return 8001
    }
}

module.exports = conf()
