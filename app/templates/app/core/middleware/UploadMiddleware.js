'use strict'

let uploadUrl = require('../../config/urls').uploads

let multer = require('multer')

let storage = multer.diskStorage({
  destination: function (req, file, cb) {
  	req.file = file
      console.log("Upload url is ", uploadUrl)
    cb(null, uploadUrl)
  }
})

module.exports = multer({ 
	storage: storage
})