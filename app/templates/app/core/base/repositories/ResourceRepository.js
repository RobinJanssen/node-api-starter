'use strict'
let EventEmitter = require('events').EventEmitter
let EVENTS = require('../../constants/events')

const ResourceRepository = function() {
	return Object.assign(EventEmitter.prototype,{
		_model: {}, //Always override this in inherited objects
		//Public methods
		find: find,
		findOne: findOne,
		create: create,
		update: update,
		destroy: destroy
	})
}
//Implementation

//Find by args
function find(args){
	let _docs
	, {limit,offset} = args

	//Find based on args, limit and offset
	return this._model.find()
		.skip(offset)
		.limit(limit)
		.exec()
		.then((docs)=> {
			_docs = docs
			return this._model.count()
		})
		//Count the total number of docs within the params
		.then(count=>{
			return {
				items: _docs,
				offset: offset,
				limit: limit,
				total: count
			}
		})
		.catch(e=>{throw e})
}
//Find by id
function findOne(id){
	return this._model.findOne({_id: id})
		.then(doc=>({item:doc}))
		.catch(e=>{throw e})
}

//Create new resource, requires a body object with the fields of the model
function create(body){
	//Set dates for creation and updates
    body.created_at = new Date();
    body.updated_at = new Date();

	return _doCreate.apply(this,[this._model,body])
}
//Update a resource by id
function update(body, id){
	//Set dates for updates
	body.updated_at = new Date();

	return _doUpdate.apply(this,[this._model, id, body])
}
//Destroy a resource by id
function destroy(id){
	let _doc;
	return this._model.findOne({_id: id})
		.then((doc)=> {
			_doc = doc
			return doc.remove()
		})
		.then(s=>{
			//Emits for listeners
			this.emit(EVENTS.DELETED_RESOURCE,_doc)
			return {
				deleted: _doc
			}
		})
		.catch(e=>{throw e})
}
//Private create
function _doCreate(model, data){
    return model.create(data)
        .then(doc=>{
			//Emits for listeners
            this.emit(EVENTS.CREATED_RESOURCE,doc)
            return {
                item: doc
            }

        })
		.catch(e=>{throw e})
}
//Private update method
function _doUpdate(model, id, data){
    return model.findOneAndUpdate({_id:id}, data)
        .then(doc=> {
			//Emits for listeners
			this.emit(EVENTS.SAVED_RESOURCE,doc)
			return {old: doc}
		})
		.catch(e=>{throw e})
}



module.exports = ResourceRepository()

