'use strict'

const User	= require('../../models/User')

const BasicRestAuthMiddleware = function(req, res, next){

  if (req.method==='OPTIONS') {
    res.status(200).send();
  }
  if(req.method==='GET'){
    return next()
  }

  if (req.isAuthenticated()) {
    User.findOne({_id:req.user._id}, function(err, user){
      if (null!==err) {
        res.send({
          ok : false,
          status : 401,
          error : 'Unauthorized',
          error_description: "User is not authenticated"
        })
      }

      req.user.role = user.role
      req.session.user = user

      next()
    });
  } else {
    res.status(401)
    res.send({
      ok : false,
      status : 401,
      error : 'Unauthorized',
      error_description: "User is not authenticated"
    })
  }
}
module.exports = BasicRestAuthMiddleware