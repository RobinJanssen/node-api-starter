'use strict'
let ensureAuthenticated = require('../middleware/BasicRestAuthMiddleware')
let router = require('express').Router()

//Implementation of REST pattern
const RestRouter = Object.assign(router,{
    resource(resourceName, controller, auth){
        router.route(`/${resourceName}`)
          .all((req,res,next)=>{
            if(auth)
              ensureAuthenticated(req,res,next)
            else
              next()
          })
          .get((req,res,next)=>{
              controller.index(req,res,next)
          })
          .post((req,res,next)=>{
              controller.store(req,res,next)
          })
        router.route(`/${resourceName}/:id`)
          .all((req,res,next)=>{
            if(auth)
              ensureAuthenticated(req,res,next)
            else
              next()
          })
          .get((req,res,next)=>{
                controller.show(req,res,next)
            })
            .put((req,res,next)=>{
                controller.update(req,res,next)
            })
            .delete((req,res,next)=>{
                controller.destroy(req,res,next)
            })
    }
})

module.exports = RestRouter