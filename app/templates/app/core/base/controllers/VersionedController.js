let version = require('../facades/VersionFacade')

let Controller = require('./ResourceController')()

let VersionedController = function() {
	
	return Object.assign(Object.create(Controller), {
		index: index
	})
}

//Implementation
function index(req, res, next) {
	var limit 		= parseInt(req.query.limit) || 0
	var offset 		= parseInt(req.query.offset) || 0
	var newerthen 	= parseInt(req.query.newerthen) || 0

	var query = this._model;

	version.getVersion(this._model.collection.name)
		.then(function(version){
			if(version && newerthen >= version.number){
				res.status(404).send({
		            ok : false,
		            status : 404,
		            error : "NotFoundException",
		            error_description: "There is no newer version of this resource"
				})
			} else {
				query.find({})
					.skip(offset)
					.limit(limit)
					.sort({name:1})
					.exec(function(err, result){
						if(err){
							throw err
						} else {
							query.count(function(err, count){
								var obj = {
									ok: true,
									status: 200,
									items: result,
									limit: limit,
									offset: offset,
									total: count,
									version: version ? version.number : 0
								}
								res.send(obj)
							})
						}
					})
			}
		})
}



module.exports = VersionedController