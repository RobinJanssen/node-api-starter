'use strict'

const ResourceController = ()=> {
    return {
        //Always set this in specific controllers
        _resource: {}, //Assumed to be instance of ResourceRepository
        //Public functions
        index (req, res, next){
            console.log("INDEX");
            let {query} = req || {};
            console.log(query);
            this._resource.find(query)
                .then(result=>{res.send(result)})
                .catch(e=>next(e))
        },
        show (req, res, next){
            let {id} = req.params;
            return this._resource.findOne(id)
                .then(result=>{res.send(result)})
                .catch(e=>next(e))
        },
        store(req, res, next){
            let {body} = req;
            return this._resource.create(body)
                .then(doc=>res.send(doc))
                .catch(e=>next(e))
        },
        update(req, res, next){
            let {body} = req;
            let {id} = req.params;
            return this._resource.update(body,id)
                .then(doc=>res.send(doc))
                .catch(e=>next(e))
        },
        destroy(req, res, next){
            let {id} = req.params;
            return this._resource.destroy(id)
                .then(doc=>res.send(doc))
                .catch(e=>next(e))
        }
    }
}

module.exports = ResourceController()

