'use strict'

let mongoose = require('mongoose');

let UserRoleSchema = new mongoose.Schema({
    name: String
});

module.exports = mongoose.model('UserRole', UserRoleSchema);
