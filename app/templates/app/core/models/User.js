'use strict'

let mongoose = require('mongoose')
let passportLocalMongoose = require('passport-local-mongoose');
let crypto = require('crypto');

let UserSchema = new mongoose.Schema({
	id: mongoose.Schema.ObjectId,
	uuid: String,
	username : String,
	email_address: String,
	first_name : Object,
	last_name: String,

	role: {
    	role_id: { type: mongoose.Schema.ObjectId, ref: 'UserRole'},
    	name: String
    },
    is_active: Boolean,
	is_manager: {
		type:Boolean,
		default:false
	},
	voted_movies: [{
		movie_id: mongoose.Schema.ObjectId,
		vote: Boolean
	}],
	created_by : [{
		user_id: {type: mongoose.Schema.ObjectId, ref:'User'},
		user_name: String
	}],
	updated_by : [{
		user_id: {type: mongoose.Schema.ObjectId, ref:'User'},
		user_name: String
	}],
	deleted_by : [{
		user_id: {type: mongoose.Schema.ObjectId, ref:'User'},
		user_name: String
	}],
	created_at : Date,
	updated_at : Date,
	deleted_at : Date

	//TODO: Add cms_preferences object to user
})

UserSchema.plugin(passportLocalMongoose);

UserSchema.methods.serializeUser = function(user, done) {
  done(null, user.id);
};

UserSchema.methods.deserializeUser = function(id, done) {
  User.findById(id, function(err, user) {
    done(err, user);
  });
};

module.exports = mongoose.model('User', UserSchema)