'use strict';
let express = require('express')
let app = express()
let exphbs  = require('express-handlebars')
let path = require('path')
let dbConf = require('./config/database')
let port = require('./config/port')
let sessionConf = require('./config/session')
let cors = require('./config/cors')
let morgan = require('morgan')
let bodyParser = require('body-parser')
let cookieParser = require('cookie-parser');
let passport = require('passport');
let LocalStrategy = require('passport-local').Strategy;

//Models
let User = require('./core/models/User');
require('mongoose').connect(`mongodb://${dbConf.db_host}:27017/${dbConf.db_name}`)

//Environment
app.set('env', process.env.NODE_ENV)


//Configs
app.use(morgan("combined",{}))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))
app.use(cookieParser());
app.use(require('express-session')({
    secret: sessionConf.secret,
    resave: false,
    saveUninitialized: false
}));

//Passport config
app.use(passport.initialize());
app.use(passport.session());
passport.use(new LocalStrategy(User.authenticate()));
passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());

//Allow the CMS to request from different origins
app.use('/api', cors)
app.use('/auth', cors)

//Serve public as static
app.use(express.static(path.join(__dirname, 'public')))
//Routes
app.use('/api', require('./api/routes'))
app.use('/', require('./routes'))

//Generic error handler
if (app.get('env') === 'DEV') {
    //also enable live reload
    app.use(require('connect-livereload')())
    //Dev error handler
    app.use(function(err, req, res, next) {
        console.error(err)
        console.error(new Error().stack);
        if(err.status === 404){
            render404(res)
            return;
        }
        res.status(err.status || 500);
        let obj = {
            ok : false,
            status : err.status || 500,
            error : err.error,
            error_description: err.error_description
        }
        res.send(obj);
    });
} else {
    // production error handler
    app.use(function(err, req, res, next) {
        if(err.status === 404){
            render404(res)
            return;
        }
        res.status(err.status || 500);
        let obj = {
            ok : false,
            status : err.status || 500,
            error : err.error,
            error_description: err.error_description
        }
        res.send(obj);
    });
}

//Boot
let server = app.listen(port, ()=>{
    console.log("Server listening at ", server.address().port)
})


//do something when app is closing
process.on('exit', exitHandler.bind(null,{cleanup:true}))
//catches ctrl+c event
process.on('SIGINT', exitHandler.bind(null, {exit:true}))
//catches uncaught exceptions
process.on('uncaughtException', exitHandler.bind(null, {exit:true}))
//Exit handlers
function exitHandler(options, err) {
    if (options.cleanup) console.log('clean')
    if (err) console.log(err.stack)
    if (options.exit) process.exit()
}
