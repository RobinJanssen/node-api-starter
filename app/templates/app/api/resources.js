'use strict'

// Example resource
// The value in controller has to be a controller object that has inherited from /app/core/base/controllers/ResourceController or Controller:
// {name: 'things', controller: thingController}
const resources = [
  /* yeoman hook */
  {name:'users', controller: require('./users/UserController'), authenticated:true}
]

module.exports = resources