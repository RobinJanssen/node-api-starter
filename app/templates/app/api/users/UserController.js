'use strict'
let ResourceController = require('../../core/base/controllers/ResourceController')
let User = require('../../core/models/User')

const UserController = () => {
  return Object.assign(Object.create(ResourceController),{
    _resource: User
})
}

module.exports = UserController()