'use strict'
//RestRouter is an extended express router that allows for direct REST configuration with router.resource()
let router = require('../core/base/routers/RestRouter')
let resources = require('./resources')

let uploadMiddleware 	= require('../core/middleware/UploadMiddleware');
let uploadController 	= require('./upload/UploadController');

//Restful routes based on resources config
//If you want full REST services on a url, just add it to /api/resources.js
resources.forEach(resource=>{
    router.resource(resource.name, resource.controller, resource.authenticated)
})

//Uploads
router.post('/upload', uploadMiddleware.single('file'), function(req, res, next){
    uploadController.create(req, res, next)
})

module.exports = router